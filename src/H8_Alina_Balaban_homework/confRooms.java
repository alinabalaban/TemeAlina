/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package H8_Alina_Balaban_homework;

/**
 *
 * @author Alina
 */
public class confRooms {

    int seats;
    int TV;
    int VideoProjector;
    int TelePresence;
    String roomType;

    public confRooms(BuildingFloors B, int S) {
        seats = S;
        roomType = "Conf Room";
        if (S > 10) {
            VideoProjector = 1;
            TV = 0;
        } else {
            VideoProjector = 0;
            TV = 1;
        }
        int Z = B.parseInt();
        if (Z == 3) {
            TelePresence = 1;
        }
    }

    public String toString() {
        String Tip = "A " + roomType + " with " + seats + " seats and "+TV+" Tv and "+VideoProjector+" VideoProjector and "+TelePresence+ " TelePresence" ;
        return Tip;
    }

}
