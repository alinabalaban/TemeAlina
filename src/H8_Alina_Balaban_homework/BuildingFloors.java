/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package H8_Alina_Balaban_homework;

/**
 *
 * @author Alina
 */
public class BuildingFloors {

    private int Floor;
    public Building Cladire;
    public int toilets;

    public BuildingFloors(int F, Building C) {
        Floor = F;
        Cladire = C;
        toilets=2;
    }

    public void setFloor(int F) {
        F = Floor;
    }

    public int getFloor() {
        return Floor;
    }

    @Override
    public String toString() {
        String Mesaj = "Floor " + Floor +" with "+toilets+" toilets ";
        return Mesaj;
    }
    
    public int parseInt(){
        return Floor;
    }

}
