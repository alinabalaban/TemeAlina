/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package H8_Alina_Balaban_homework;

/**
 *
 * @author Alina
 */
public class Kitchen {
    int fridge=0;
    int waterDispenser;
    int coffeeMachine;
    String roomType;
     public Kitchen(BuildingFloors B) {
         fridge=1;
         waterDispenser=1;
         coffeeMachine=1;
         roomType="Kitchen";
    }
     
    @Override
      public String toString() {
        String Tip="A "+roomType+" with "+fridge+" fridge and "+ waterDispenser+" waterDispenser and "+coffeeMachine+ " coffee machine.";
        return Tip;
    }
    
}
