/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package H8_Alina_Balaban_homework;

import java.io.IOException.*;

/**
 *
 * @author Alina
 */
public class Building {

    private String Adress;
    private String Town;

    public Building(String A, String T) throws NullPointerException {
        if (A == null || T == null) {
            throw new NullPointerException();
        }
        else{
        Adress = A;
        Town = T; 
            System.out.println(Adress+Town);}
        
    }

    public Building() throws NullPointerException {
        if (Adress != null) {
            System.out.println("New building was build");
        } else {
            throw new NullPointerException(
                    "Adresa cladirii este nula");
        }

    }

    public void setAdress(String A) {
        A = Adress;
    }

    public String getAdress() {
        return Adress;
    }

    public void setTown(String T) {
        T = Town;
    }

    public String getTown() {
        return Town;
    }

    @Override
    public String toString() {
        String InfoBuilding = "Building was created, this is the adress " + Adress + " in this town " + Town;
        return InfoBuilding;
    }
}
